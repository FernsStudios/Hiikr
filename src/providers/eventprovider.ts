﻿import { Injectable } from '@angular/core';
import firebase from 'firebase';
//!!!!! this class is from the Contractr frame, we were using it to display the list on the trail page, and may use it categorize the list from Data.ts and display it in a search function
@Injectable()
export class EventProvider {

    constructor() { }
     

    /*createEvent(trailName: string, trailLength: number, eventTools: string, eventPrice: number,): firebase.Promise<any> {
        return firebase.database()
        
            .ref(`/TrailList`)
            .push({
                name: trailName,
                date: trailLength,
                tools: eventTools,
                price: eventPrice,
            
            });
       
            
    }*/ //ignore this one, unless we want users to be able to create trails, which we might eventually
 

    getEventList(): Promise<any> {
        return new Promise((resolve, reject) => {
            firebase.database()
                .ref(`/TrailList`)
                .on('value', snapshot => {
                    let rawList = [];
                    snapshot.forEach(snap => {
                        rawList.push({
                        
                            id: snap.key,
                            Difficulty: snap.val().Difficulty,
                            Hours: snap.val().Hours,
                            TrailLength: snap.val().TrailLength,
                            TrailName: snap.val().TrailName,
                           
                        });
                        return false
                    });
                    resolve(rawList);
                });
        });
    }//end of getEventList, this calls the info at traillist node, and snaps the info to an item that is importeed at rawList.push





    getEventDetail(eventId): Promise<any> {
        return new Promise((resolve, reject) => {
            firebase.database()
                .ref(`/eventList`)
                .child(eventId).on('value', snapshot => {
                    resolve({
                        id: snapshot.key,
                        name: snapshot.val().name,
                        date: snapshot.val().date,
                        price: snapshot.val().prive,
                        revenue: snapshot.val().revenue
                    });
                });
        });
    }// used to be called when they clicked on an item to see more info on it



   
}//end of eventprovider