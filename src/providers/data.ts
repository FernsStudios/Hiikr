﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

//This class holds the list for the trails search function
@Injectable()

export class Data {
    items: any;
    constructor() {

        this.items = [
            { title: 'Dragoon Gulch', image: '', difficulty: 'Easy', location: 'Sonora', length: '1.6 miles'},
            { title: 'Pinecrest', image: '', difficulty: 'Medium', location: 'Sonora', length: '1.6 miles'},
            { title: 'Red Hills', image: '', difficulty: 'Hard', location: 'Sonora', length: '1.6 miles' },
            { title: 'Table Top', image: '', difficulty: 'Hard', location: 'Sonora', length: '1.6 miles'},
            { title: 'Lyons Dam', image: '', difficulty: 'Intermediate', location: 'Sonora', length: '1.6 miles' },
            { title: 'Stevies Moms House', image: '', difficulty: 'Easy', location: 'Sonora', length: '1.6 miles' }
        ]

    }

    filterItems(searchTerm) {

        return this.items.filter((item) => {
            return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });

    }

}//end of Data