﻿//component imports
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
//tabs pages
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
//ionic imports
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//authentication pages
import { Login } from '../pages/login/login';
import { Signup } from '../pages/signup/signup';
import { Resetpassword } from '../pages/resetpassword/resetpassword';
import { Profile } from '../pages/profile/profile';
//providers for authentication and profiles
import { AuthProvider } from '../providers/auth';
import { ProfileProvider } from '../providers/profile';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { EmailValidator } from '../validators/email';
import { EventProvider } from '../providers/eventprovider';
import { Data } from '../providers/data';
//trail pages
import { DragoonGulch } from '../pages/dragoon-gulch/dragoon-gulch';
import { PinecrestPage } from '../pages/pinecrest/pinecrest';
import { RedHillsPage } from '../pages/red-hills/red-hills';
import { TableTopPage } from '../pages/table-top/table-top';

//initalize fire base 
const firebaseConfig = {
    apiKey: "AIzaSyBAL5UoF8blxVEKyi2hsa0De-LfS7uN2mY",
    authDomain: "hiikr-83c50.firebaseapp.com",
    databaseURL: "https://hiikr-83c50.firebaseio.com",
    projectId: "hiikr-83c50",
    storageBucket: "hiikr-83c50.appspot.com",
    messagingSenderId: "1033520671859"
};


@NgModule({

  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
      TabsPage,
      Login,
      Signup,
      Resetpassword,
      Profile,
      DragoonGulch,
      PinecrestPage,
      TableTopPage,
      RedHillsPage

    
    ],


  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule
  ],


  bootstrap: [IonicApp],


  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Login,
    Signup,
    Resetpassword,
      Profile,
    DragoonGulch,
    PinecrestPage,
    TableTopPage,
    RedHillsPage
  ],


  providers: [
      Data,
    EventProvider,
    AuthProvider,
    ProfileProvider,
    StatusBar,
    SplashScreen,
   {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})

export class AppModule {}
