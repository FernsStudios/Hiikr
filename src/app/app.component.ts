﻿import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { Signup } from '../pages/signup/signup';
import { HomePage } from '../pages/home/home';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, afAuth: AngularFireAuth) {
      const authObserver = afAuth.authState.subscribe(user => {
          if (user) {
              this.rootPage = TabsPage;
              authObserver.unsubscribe();
          } else {
              this.rootPage = Signup;
              authObserver.unsubscribe();
          }
      });//this listens for auth changes, if there is a logged in user the app will send them to homepage, if not they get sent to log in page
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
