﻿import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home'; 
import { Profile } from '../profile/profile';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
    //this sets the three tabs on our page, if we want more we need to define more roots and call them here. 
    //the visual stylings and icons are in the html
  tab1Root = AboutPage;
  tab2Root =  HomePage;
  tab3Root = Profile;

  constructor() {

  }
}
