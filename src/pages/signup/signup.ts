﻿import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, Loading,AlertController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth';
import { EmailValidator } from '../../validators/email';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs'
import { Login } from '../login/login';
/**
 this class and the login class are super similar and do similar things, this also creates info at /user node in our firebase database, while creating the auth profile
 */

@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})

export class Signup {
    //init variables
    public signupForm: FormGroup;
    public loading: Loading;
    constructor(public navCtrl: NavController, public navParams: NavParams, public authData: AuthProvider,
        public formBuilder: FormBuilder, public loadingCtrl: LoadingController,
        public alertCtrl: AlertController) {
        this.signupForm = formBuilder.group({
            email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
            password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
           

        });
    }//end of constructor. this holds a form group that validates email@somewhere.com, and pass is over 6 characters

    signupUser() {
        if (!this.signupForm.valid) {
            console.log(this.signupForm.value);
        } else {
            this.authData.signupUser(this.signupForm.value.email, this.signupForm.value.password,)
                .then(() => {
                    this.navCtrl.setRoot(TabsPage);
                }, (error) => {
                    this.loading.dismiss().then(() => {
                        var errorMessage: string = error.message;
                        let alert = this.alertCtrl.create({
                            message: errorMessage,
                            buttons: [
                                {
                                    text: "Ok",
                                    role: 'cancel'
                                }
                            ]
                        });
                        alert.present();
                    });
                });

            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: true,
            });
            this.loading.present();
        }
    }//end of sign up user, basically the same as login but it also creates a new authentication profile at our fire base

    gotoLogin()
    {
        this.navCtrl.push(Login);
    }//pushes to login page

}//end of Singnup