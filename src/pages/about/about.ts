﻿import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EventProvider } from '../../providers/eventprovider';
import {DragoonGulch} from '../dragoon-gulch/dragoon-gulch';
import {PinecrestPage} from '../pinecrest/pinecrest';
import { RedHillsPage } from '../red-hills/red-hills';
import { TableTopPage } from '../table-top/table-top';
import { LyonsDamPage } from '../lyons-dam/lyons-dam';
import { Data } from '../../providers/data';
import { FormControl } from '@angular/forms';
import firebase from 'firebase';
import 'rxjs/add/operator/debounceTime';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

//this page displays, currently, all the trails
export class AboutPage {
  

    public trailList: Array<any>;
    public loadedTraillist: Array<any>;
    public trailRef: firebase.database.Reference;

    constructor(public navCtrl: NavController, public eventProvider: EventProvider, public dataService: Data) {
      //okay here we go 
        this.trailRef = firebase.database().ref('/trailList');
        //this is grabbing our refrence in our firebase database, trail list

        this.trailRef.on('value', trailList => {
            //this pushes all the value under each node in the trail list
            let trails = [];
            //creates the array
            trailList.forEach(trail => {
                trails.push(trail.val());
                return false;
            }); //push all the value to the array

            this.trailList = trails;
            this.loadedTraillist = trails; //push the array to our loaded list
        });


    }
    initializeItems(): void {
        this.trailList = this.loadedTraillist; //and this pushes the array to the view page
    }
    getItems(searchbar) {
        // Reset items back to all of the items
        this.initializeItems();

        // set q to the value of the searchbar
        var q = searchbar.srcElement.value;


        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }

        this.trailList = this.trailList.filter((v) => {
            if (v.title && q) {
                if (v.title.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
        });

        console.log(q, this.trailList.length);

    } //this function sends the item to the query in the searchbar, and creates the items to list and filter



    


    

   

 
    //Trail Page Links
    goToDragoonGulchPage()
    {
        this.navCtrl.push(DragoonGulch)
    }
    goToPinecrestPage()
    {
        this.navCtrl.push(PinecrestPage)
    }
    goToRedHillsPage()
    {
        this.navCtrl.push(RedHillsPage)
    }
    goToTableTopPage()
    {
        this.navCtrl.push(TableTopPage)
    }
    goToLyonsDamPage()
    {
        this.navCtrl.push(LyonsDamPage)
    }
}//END OF ABOUTPAGE
