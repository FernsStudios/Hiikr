﻿import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
//imports for auth system
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth';
import { TabsPage } from '../tabs/tabs';
import { EmailValidator } from '../../validators/email';
import { Resetpassword } from '../resetpassword/resetpassword';
import { Signup } from '../signup/signup';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})


export class Login {

//init variables
    loginForm: FormGroup;
    loading: Loading;
    //form group will create the validation system, requiring a full email@somewhere.com, and 6 digits for password
    constructor(public navCtrl: NavController, public navParams: NavParams, public authData: AuthProvider,
    public formBuilder: FormBuilder, public alertCtrl: AlertController, public loadingCtrl: LoadingController)
    {
         this.loginForm = formBuilder.group
            ({
            email: ['', Validators.compose([Validators.required,
            EmailValidator.isValid])],
            password: ['', Validators.compose([Validators.minLength(6),
            Validators.required])]
            });//this is the set for email/pass

    }//end of constructor
    





    goToResetPassword()
    {
        this.navCtrl.push(Resetpassword);
    } //pushes to ResetPassword page

    createAccount()
    {
        this.navCtrl.push(Signup);
    }//pushes to Signup Page


    loginUser()
    {
        if (!this.loginForm.valid) {
            console.log(this.loginForm.value);
            //if the formgroup doesnt match up it will reset and tell the user there was an error
        } else {
            this.authData.loginUser(this.loginForm.value.email, this.loginForm.value.password)
                .then(authData => {
                    this.navCtrl.setRoot(TabsPage);
                }, error => {
                    this.loading.dismiss().then(() => {
                        let alert = this.alertCtrl.create({
                            message: error.message,
                            buttons: [
                                {
                                    text: "Ok",
                                    role: 'cancel'
                                }
                            ]
                        });
                        alert.present();
                    });
                });

            this.loading = this.loadingCtrl.create
            ({
                dismissOnPageChange: true,
            });
            this.loading.present();
        }//end of else block
        //the else block basically defines that the user is now a logged in user, then sets their root page to the tabs page rather then the create an account page. 
        //the other thing it SHOULD do is retain the users logged info and return them to the tabs page when they reopen the app. I think

    }//end of LoginUser, this sends the user in and keeps their root as homepage as long as their logged in as an authenticated user in our firebase



}//end of login
